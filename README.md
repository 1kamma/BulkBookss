# Bulk Books:

This script's goal is to help you to download books from [Kotar](https://kotar.cet.ac.il/).

## How To?
1. You need an Academic Access to Kotar.
1. You need to have python>=3.9
1. download the requiremetns. (It might be prefered by using venv).
1. Add the links to the __BooksToDownload__ file.
1. Run the script.

Enjoy.